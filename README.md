# TS Linter

## Prepare

`npm i`

## Demo

- Example running from JS `node js/index.js demoFiles`

## TODO

- [ ] More example demo files
- [ ] (fix) Example running from TS `npm start`
- [ ] Option to auto-fix problems
- [ ] Detect error line number on file
- [ ] Setup a file configuration, not passing as arguments
- [ ] Add tests
