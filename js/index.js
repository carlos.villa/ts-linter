#!/bin/node 

const fs = require('fs')
const _ = require('lodash')

// asignacion dentro de if
const checkForIncorrectIfAssignments = {
  rule: /.*if\s*\([^=]*=[^=][^)]*\).*/g,
  message: 'asignacion dentro de if'
}
// comparacion con doble ==
const checkForLooseEquals = {
  rule: /.*[^=]==[^=].*/g,
  message: 'comparacion con doble =='
}
// multiples espacios entre palabras
const checkForMultipleSpacesInMiddle = {
  rule: /.*\w+\s\s\w+.*/g,
  message: 'multiples espacios entre palabras'
}
// constantes con minusculas
const checkForContantsLowerCase = {
  rule: /const\s+[a-z]+.*/g,
  message: 'constantes con minusculas'
}
// funcion comienza con mayuscula
const checkForFunctionsUpperCase = {
  rule: /function\s+[A-Z]+[a-zA-Z0-9]+.*/g,
  message: 'funcion comienza con mayuscula'
}

const verboseDebug = true

function _log(...params) {
  if (verboseDebug)
    console.dir(params)
}

const incorrectAssignments = [
  checkForIncorrectIfAssignments,
  checkForLooseEquals,
  checkForContantsLowerCase,
  checkForFunctionsUpperCase,
  checkForMultipleSpacesInMiddle
]

const args = process.argv
const filesToCheck = args.slice(2)
let content = filesToCheck.map(file => {
  _log('Scanning file: ', file)
  let data = fs.readFileSync(file).toString()
  // _log('data', data)
  return data
})

let regexErrorApply = content.map(content => {
  return incorrectAssignments.map(test => {
    let result = content.match(test.rule)
    return { result, test }
  })
})

// regexErrorApply is a matrix
regexErrorApply = _.flatten(regexErrorApply)
// filter only 
let rulesWithErrors = _.filter(regexErrorApply, error => error.result)
console.dir(rulesWithErrors)

rulesWithErrors.forEach((invalid, index, arr) => {
  console.log(
    'ERROR: ' +
    invalid.test.message +
    '\nrule: ' +
    invalid.test.rule
  )
  _.forEach(invalid.result, result => {
    console.error(
      '\ton line: ' + result
    )
  })
  if (index == arr.length - 1) {
    process.exit(1)
  }
})
