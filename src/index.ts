#!/usr/bin/env node

import { config } from 'dotenv'
import program from 'commander'
import colors from 'colors'
// import { readFileSync } from 'fs'
import constants from './const'
import { } from './lib/function'

config()
const packageJson = require('../package.json')

function _log (...params) {
  if (constants.LOGGER) {
    console.dir(params)
  }
}

function lint (options) {
  _log(colors.red('options'), colors.green(options))
  process.exit(0)
}

program
  .version(packageJson.version)
  .option('-d, --direcotry', 'Lint files in directory')
  .option('-v, --verbose [optional]', 'Verbose logging')
  .description('Check for lint errors on specified directory')
  .action(function (options) {
    console.log('inside lint', options)
    lint(options)
  })

program
  .on('command:*', function () {
    console.error(('Invalid command ' + program.args[0]).red)
    program.outputHelp()
  })

program.parse(process.argv)

// if (program.args.length < 1) {
//   program.outputHelp()
// }

export default {

}
